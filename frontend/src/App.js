import {BrowserRouter, Routes, Route} from 'react-router-dom'

import Student from './component/Student';
import Recordtable from './component/Recordtable';
import UpdateStudent from './component/UpdateStudent';
import Login from './component/login';
import Signup from './component/signup';
import Navbarn from './component/navbar';
function App() {
  return (
    
    <div className='App'>
      
<BrowserRouter>
<Routes>

<Route path='/' element={<Recordtable/>}></Route>
{/* <Route path='/' element={<Navbarn/>}></Route> */}
  <Route path='/create' element={<Student/>}></Route>
  <Route path='/update/:id' element={<UpdateStudent/>}></Route>
  <Route path='/login' element={<Login/>}></Route>
  {/* <Route path='/signup' element={<Signup/>}></Route> */}
</Routes>
</BrowserRouter>
    </div>
  );
}

export default App;
