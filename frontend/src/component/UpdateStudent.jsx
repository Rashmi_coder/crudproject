import React, { useEffect, useState } from 'react'
import axios from 'axios';
import { useNavigate, useParams } from 'react-router-dom';
export default function UpdateStudent() {
  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');
  const [dob, setDOB] = useState('');
  const { id } = useParams();
  const navigate = useNavigate();
  const [file, setfile] = useState(0);

  const handleFile = (e) => {
    setfile(e.target.files[0])
  }
  const handleUpload = () => {

    const formdata = new FormData();

    formdata.append('name', name);
    formdata.append('phone', phone);
    formdata.append('dob', dob);
    formdata.append('image', file);
    console.log("image upload", formdata)
    axios.put('http://localhost:8081/update/' + id, formdata)
      .then(res => {
        console.log(res);
      // window.location.href("/")
          navigate('/')
          console.log("object")
      })
      .catch(err => console.log(err))
  }

  useEffect(() => {
    axios.get(`http://localhost:8081/edit/` + id)
      .then(res => {
        console.log(res)
        setName(res.data[0].name);
        setPhone(res.data[0].phone);
        setDOB(res.data[0].dob);
      })
      .catch(err => console.log(err))
  }, [id])

  return (
    <div className="flex justify-center pt-10">
      <div className="bg-black px-4 pt-4 pb-[0.05rem] bg-opacity-30 rounded-xl ">
        <div className='bg-slate-800 text-center px-2 py-2 rounded-md border-2 border-red-700'>

          <p className='text-white md:text-lg'>Edit Your Details</p>
        </div>

        {/* form */}
        <form className='my-2' encType="multipart/form-data">
          <input type="text" name="name" value={name} onChange={(e) => setName(e.target.value)} placeholder='Enter Your Name' className='w-full bg-white text-black border border-black-200 rounded py-1.5 px-2 mb-2 focus:outline-none ' required />
          <input type="number" name="phone" value={phone} onChange={(e) => setPhone(e.target.value)} placeholder='Enter Your Phone Number' className="w-full bg-white text-black border border-black-200 rounded py-1.5 px-2 mb-2 focus:outline-none" required />
          <div className='md:flex md:space-x-3'>
            <input type="text"
              name="dob"
              required
              value={new Date(dob).toLocaleDateString('es-CL')}
              onChange={(e) => setDOB(e.target.value)}
              className="w-full bg-white text-black border border-black-200 rounded py-1.5 px-2 mb-2 focus:outline-none" />
          </div>
          <input type='file' name='image' onChange={handleFile} />
          <br />
          {/* {submit and attachment} */}

          <div className="flex space-x-2 py-0.5">
            <div className='w-1/2'>
              <button type="submit" className="bg-red-700 text-white w-full py-2 rounded" onClick={(e)=>{e.preventDefault();handleUpload()}}>
                Update
              </button>
            </div>
          </div>
        </form>
        {/* form end */}
      </div>

    </div>
  )
}
