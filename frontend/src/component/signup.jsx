import React, { useState } from 'react'
import axios from 'axios';

export default function Signup() {
    const [usernamereg, setUsername] = useState('');
    const [passreg,setPass]=useState('');
    const[msg,setmsg]=useState('')
    const register=()=>{
        axios.post("http://localhost:8081/register", {username: usernamereg, password:passreg})
        .then(res=>{
            console.log(res)
            setmsg("Signup successfully")
        })
    }
  return (
    <div>
      <div className='flex justify-center'>
      <div className="w-full max-w-xs ">
      <h1 className='flex justify-center font-bold text-lg'>Signup Form</h1>
  <form className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
    <div className="mb-4">
      <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
        Username
      </label>
      <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text" placeholder="Username" onChange={(e)=>{setUsername(e.target.value)}}/>
    </div>
    <div className="mb-6">
      <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="password">
        Password
      </label>
      <input className="shadow appearance-none border border-red-500 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" id="password" type="password" placeholder="******************" onChange={(e)=>{setPass(e.target.value)}}/>
      <p className="text-red-500 text-xs italic">Please choose a password.</p>
    </div>
    <div className="flex items-center justify-between">
      <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="button" onClick={register}>
        Register
      </button>
    </div>
    {msg??<span>{msg}</span>}
  </form>
</div>
    </div>
    </div>
  )
}
