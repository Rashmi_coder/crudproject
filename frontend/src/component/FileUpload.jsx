import React, { useEffect, useState } from 'react'
import axios from 'axios';
export default function FileUpload() {
    const [data, setData] = useState([]);
    const[file,setfile]=useState(0);
    const handleFile=(e)=>{
setfile(e.target.files[0])
    }
    const handleUpload=()=>{
        const formdata = new FormData();
        formdata.append('image',file);
        axios.post('http://localhost:8081/upload', formdata)
        .then(res=> {
            if(res.data.Status==="success"){
                console.log("success")
            }else{
                console.log("failed")
            }
        })
        .catch(err=>console.log(err))
    }
    useEffect(() => {
        axios.get('http://localhost:8081/')
            .then(res => {
                console.log(res);
                setData(res.data[0])})
            .catch(err => console.log(err))
    }, [])
  return (
    <div>
      <input type='file' onChange={handleFile}/>
      <button onClick={handleUpload}>Upload</button>
      <br/>
      <img src={`http://localhost:8081/images/`+data.image} alt='' width="50" height="50"></img>
    </div>
  )
}
