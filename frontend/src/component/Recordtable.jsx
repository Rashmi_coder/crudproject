import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { FaEdit } from "react-icons/fa";
import { MdDeleteForever } from "react-icons/md";
import { Link } from 'react-router-dom';

export default function Recordtable() {
    const [list, setList] = useState([]);
    useEffect(() => {
        axios.get('http://localhost:8081/')
            .then(res => {
                console.log(res);
                setList(res.data)})
            .catch(err => console.log(err))
    }, [])
    
    const handleDelete=async(id)=>{
try{
    await axios.delete('http://localhost:8081/student/'+id)
    window.location.reload()

}catch(err){
    console.log(err)
}
    }
    return (
        <div className="flex justify-center py-20">
            <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div className='mb-4'>
                <Link to="/login" className="mr-4 inline-block rounded bg-blue-600 px-6 pb-2 pt-2.5 text-xs font-medium uppercase leading-normal text-white shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out hover:bg-primary-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-primary-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-primary-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] dark:shadow-[0_4px_9px_-4px_rgba(59,113,202,0.5)] dark:hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)] dark:focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)] dark:active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)]">Login</Link>
                <Link to="/signup" className="inline-block rounded bg-blue-600 px-6 pb-2 pt-2.5 text-xs font-medium uppercase leading-normal text-white shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out hover:bg-primary-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-primary-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-primary-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] dark:shadow-[0_4px_9px_-4px_rgba(59,113,202,0.5)] dark:hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)] dark:focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)] dark:active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)]">Signup</Link>
                </div>
                <div className="inline-block min-w-full py-2 sm:px-6 lg:px-8 border">
                    <div className="overflow-hidden">
                        <Link to="/create"
                            className="inline-block rounded bg-blue-600 px-6 pb-2 pt-2.5 text-xs font-medium uppercase leading-normal text-white shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out hover:bg-primary-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-primary-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-primary-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] dark:shadow-[0_4px_9px_-4px_rgba(59,113,202,0.5)] dark:hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)] dark:focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)] dark:active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)]">
                            Add +
                        </Link>

                        <table className="min-w-full text-center text-sm font-light">
                            <thead
                                className="border-b bg-neutral-800 font-medium text-white dark:border-neutral-500 dark:bg-neutral-900">
                                <tr>
                                    <th scope="col" className=" px-6 py-4">Id</th>
                                    <th scope="col" className=" px-6 py-4">Image</th>
                                    <th scope="col" className=" px-6 py-4">Name</th>
                                    <th scope="col" className=" px-6 py-4">Phone</th>
                                    <th scope="col" className=" px-6 py-4">Date-of-Birth</th>
                                    <th scope="col" className=" px-6 py-4">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {list.map((data, i) => (

                                    <tr className="border-b dark:border-neutral-500" key={i}>
                                        <td className="whitespace-nowrap  px-6 py-4 font-medium">{data.ID}</td>
                                        <td>
                                            <img src={`http://localhost:8081/images/`+data.image} alt='' width="50" height="50"></img>
                                        </td>
                                        <td className="whitespace-nowrap  px-6 py-4">{data.name}</td>
                                        <td className="whitespace-nowrap  px-6 py-4">{data.phone}</td>
                                        <td className="whitespace-nowrap  px-6 py-4">{new Date(data.dob).toLocaleDateString("es-CL")}</td>
                                        <td className='flex flex-nowrap px-6 py-4'>
                                            <Link to={`/update/${data.ID}`}><FaEdit className="text-blue-700 mr-2 "/></Link>
                                            <MdDeleteForever className="text-lg text-red-700" onClick={(e)=>handleDelete(data.ID)}/>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                ))}


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    )
}
