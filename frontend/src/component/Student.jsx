import React, { useState } from 'react'
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
export default function Student() {
  
 const[name, setName]=useState(''); 
 const[phone, setPhone]=useState(''); 
 const[dob, setDOB]=useState(''); 


  const navigate = useNavigate();

  function handleSubmit(event){
    event.preventDefault();
    axios.post('http://localhost:8081/create', {name, phone, dob})
    .then(res=> {
      console.log(res);
      navigate('/')
    })
    .catch(err=> console.log(err))
  }
  return (
    <div className="flex justify-center pt-10">
      <div className="bg-black px-4 pt-4 pb-[0.05rem] bg-opacity-30 rounded-xl ">
        <div className='bg-slate-800 text-center px-2 py-2 rounded-md border-2 border-red-700'>

          <p className='text-white md:text-lg'>Add Your Details</p>
        </div>

        {/* form */}
        <form onSubmit={handleSubmit} className='my-2' encType="multipart/form-data">
          <input type="text" name="name" onChange={(e)=>setName(e.target.value)} placeholder='Enter Your Name' className='w-full bg-white text-black border border-black-200 rounded py-1.5 px-2 mb-2 focus:outline-none ' required />
          <input type="number" name="phone" onChange={(e)=>setPhone( e.target.value)} placeholder='Enter Your Phone Number' className="w-full bg-white text-black border border-black-200 rounded py-1.5 px-2 mb-2 focus:outline-none" required />
          <div className='md:flex md:space-x-3'>
            <input type="date"
              name="dob"
              required
              onChange={(e)=>setDOB(e.target.value)}
             className="w-full bg-white text-black border border-black-200 rounded py-1.5 px-2 mb-2 focus:outline-none" />
          </div>


          {/* {submit and attachment} */}

          <div className="flex space-x-2 py-0.5">
            <div className='w-1/2'>
              <button type="submit" className="bg-red-700 text-white w-full py-2 rounded">
                SUBMIT
              </button>
            </div>
          </div>


        </form>
        {/* form end */}
      </div>

    </div>
  )
}
