let express =require('express');
let cors= require('cors');
let mysql = require('mysql');  
const app = express();
let multer = require('multer');
let path = require('path')

app.use(cors());
app.use(express.json());
app.use(express.static('public'));
const db = mysql.createConnection({
    host: "localhost",
    user:"root",
    password: "",
    database: "crud"
})

const storage = multer.diskStorage({
    destination:(req, file, cb)=>{
        cb(null, 'public/images')
    },
    filename: (req, file, cb)=>{
        cb(null, file.fieldname+"_"+ Date.now() + path.extname(file.originalname))
    }
})

const upload = multer({
    storage: storage
})

app.get("/", (req,res) => {
    const sql = "select * from student";
    db.query(sql, (err, data)=>{
        if(err) return res.json(err);
        return res.json(data);
    })
});
app.post('/create', (req,res)=>{
    const sql = "INSERT INTO student(`name`, `phone`, `dob`) VALUES (?)";
    const values1 = [req.body.name, req.body.phone, req.body.dob]
    db.query(sql, [values1], (err, data)=>{
        if(err) return res.json(err);
        return res.json(data);
    })
});
app.get('/edit/:id', (req,res)=>{
    const sql = "SELECT * FROM student where ID = ?";
    // const sql = "update student set `name`=?, `phone`=?, `dob`=? where ID=?";
    // const values1 = [req.body.name, req.body.phone, req.body.dob]
    const id = req.params.id;
    db.query(sql,[id], (err, data)=>{
        if(err) return res.json({Error:err});
        return res.json(data);
    })
})

app.put('/update/:id',upload.single('image'), (req,res)=>{
    // const image = req.file.filename;
    console.log(req.body,"body================",req.file)
    console.warn( req.params.id)
    const sql = "UPDATE student SET name= '" +
    req.body.name +"', phone='"+
    req.body.phone+"', dob='"+
    req.body.dob+"', image='"+
    req.file.filename+"' where ID=" +
    req.params.id;
    // const values1 = [req.body.name, req.body.phone, req.body.dob]
    // const id = req.params.id;
    db.query(sql, (err, data)=>{
        if(err) {res.send({status:false, message: "Student update failed"});
    }else{
        res.send({status:true, message: "Student updated successfully"});
    }
    })
})

app.delete('/student/:id', (req,res)=>{
    const sql = "DELETE FROM student where ID=" + req.params.id + "";
    db.query(sql, (err)=>{
        if(err) {res.send({status:false, message: "Student delete failed"});
    }else{
        res.send({status:true, message: "Student record deleted successfully"});
    }
    })
})

app.post('/register',(req, res)=>{
    const username = req.body.username
    const password = req.body.password
    db.query("INSERT INTO users (username, password) VALUES (?,?)", [username, password],(err, res)=>{
        console.log(err);
    })
})

app.post('/login',(req, res)=>{
    const username = req.body.username
    const password = req.body.password
    db.query("SELECT * FROM users WHERE username=? AND password=?", [username, password],(err, result)=>{
        if(err){
        res.send({err: err})
    }else{
        if(result){
            res.send(result)
        }else{
            res.send({message: "Wrong username/password combination"})
        }
    }

    })
})

// app.post('/upload',upload.single('image'),(req,res)=>{
// const image = req.file.filename;
// const sql = "UPDATE student SET image = ?";
// db.query(sql, [image], (err, result)=>{
//     if(err) return res.json({Message: "Error"});
//         return res.json({Status: "Success"});
// })
// })



app.listen(8081, ()=>{
    console.log("listening")
} )